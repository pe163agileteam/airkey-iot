#include <ESP8266HTTPClient.h>
#include <EEPROM.h>
#include <Servo.h>
#include <ESP8266WiFi.h>
#include <Scheduler.h>
#include <ESP8266HTTPClient.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <Adafruit_NeoPixel.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
# define LED D5
# define OUT D2
#define LOCK_OPENED_DEGREES 69
#define LOCK_CLOSED_DEGREES 170
Adafruit_NeoPixel strip = Adafruit_NeoPixel(1, LED, NEO_GRB + NEO_KHZ800);
const int COLOR_NONE = 0;
const int COLOR_ORANGE = 1;
const int COLOR_RED = 2;
const int COLOR_GREEN = 3;
const int COLOR_BLUE = 4;
Servo servo;
const char* mqtt_server = "m16.cloudmqtt.com";
WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;
WiFiManager wifiManager;
const char* mqtt_user = "CC50E3566FDD";
const char* mqtt_password = "test";
const int port = 11031;
const char* closed = "close";
const char * opened = "open";
const int autoLockTimeout = -1;
boolean isLocked = false;
int eeAddress = 0;
HTTPClient http;
Scheduler scheduler = Scheduler();
void resetToFactoryDefaults() {
  WiFi.disconnect(true);
  delay(2000);
  ESP.reset();
  //  hardwareReset();
}

void setup() {
  pinMode(OUT, OUTPUT);
  strip.begin();
  Serial.begin(9600);
  EEPROM.begin(512);
  Serial.printf(" ESP8266 Chip id = ");
  Serial.println(ESP.getChipId());
  Serial.printf(" ESP8266 MAC = %08X\n");
  Serial.println(WiFi.macAddress());
  setupLock();
  setColorOfIndicator(COLOR_ORANGE);
  //  resetToFactoryDefaults();
  wifiManager.autoConnect("ESP8266", "12345678");
  blink(COLOR_GREEN);
  client.setServer(mqtt_server, port);
  client.setCallback(callback);
  createLockRequest();
}

void createLockRequest() {
  http.begin("http://airkey-backend.herokuapp.com/api/v1/locks/");
  http.addHeader("Content-Type", "application/json");
  const size_t capacity = JSON_OBJECT_SIZE(3);
  DynamicJsonBuffer jsonBuffer(capacity);
  JsonObject& root = jsonBuffer.createObject();
  root["mac_address"] = WiFi.macAddress();
  root["password"] = "pass12345";
  root["secret_key"] = "12345678";
  root.printTo(Serial);
  char request[150];
  root.printTo(request);
  int httpCode = http.POST(request);
  Serial.println("Adding lock request: ");
  Serial.print(httpCode);
  http.end();
}

void callback(char* topic, byte* payload, unsigned int length) {
  printMQTTMessage(topic, payload, length);
  payload[length] = '\0';
  processCommand((char*)payload);
}

void printMQTTMessage(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}

void processCommand(char* command) {
  if (strcmp (command, opened) == 0) {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    openLock(true);
  } else if (strcmp (command, closed) == 0) {
    closeLock(true);
  } else if (strcmp(command, "status") == 0) {
    sendLockStatus();
  }
}

void sendLockStatus() {
  char * status = getLockStatusJson();
  client.publish("to_server", status);
}

char* getLockStatusJson() {
  const size_t capacity = JSON_OBJECT_SIZE(3);
  DynamicJsonBuffer jsonBuffer(capacity);

  JsonObject& root = jsonBuffer.createObject();
  root["status"] = "OK";
  root["mac_address"] = WiFi.macAddress();
  root["state"] = isLocked ? "locked" : "unlocked";

  char status[150];
  root.printTo(status);
  return status;
}

void reconnectMQTT() {
  // Loop until we're reconnected
  while (!client.connected()) {
    setColorOfIndicator(COLOR_ORANGE);
    Serial.print("Attempting MQTT connection...");
    if (client.connect("ESP8266Client", mqtt_user, mqtt_password)) {
      blink(COLOR_GREEN);
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("to_server", "hello world init from NODEMCU");
      client.subscribe(mqtt_user);
      changeLockStateFromMemory(true);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      blink(COLOR_ORANGE);
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
void loop() {
  scheduler.update();
  if (!client.connected()) {
    reconnectMQTT();
  }
  client.loop();
}

void sendTestMQTTMessage() {
  long now = millis();
  if (now - lastMsg > 2000) {
    lastMsg = now;
    ++value;
    snprintf (msg, 75, "hello world #%ld", value);
    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish("to_server", msg);
  }
}

void sendHttpRequestTest() {
  HTTPClient http;
  http.begin("http://checkip.dyndns.com/");
  int httpCode = http.GET();
  if (httpCode == 200) {
    Serial.println(httpCode);
    String cIp;
    cIp = http.getString();
    Serial.println(cIp);
    setColorOfIndicator(COLOR_GREEN);
  } else {
    setColorOfIndicator(COLOR_RED);
  }
  http.end();
}

void setupLock() {
  servo.attach(2);
  changeLockStateFromMemory(false);
}

void changeLockStateFromMemory(boolean indicate) {
  boolean isLockedInMemory;
  EEPROM.get(eeAddress, isLockedInMemory);
  if (isLockedInMemory) {
    closeLock(indicate);
  } else {
    openLock(indicate);
  }
}



void openLock(boolean indicate) {
  servo.write(LOCK_OPENED_DEGREES);
  if (indicate) {
    setColorOfIndicator(COLOR_GREEN);
  }
  if (autoLockTimeout > 0) {
    scheduler.schedule(closeLockIndicate, autoLockTimeout);
  }
  isLocked = false;
  saveCurrentState();
  sendLockStatus();
}

void closeLockIndicate() {
  closeLock(true);
}

void closeLock(boolean indicate) {
  servo.write(LOCK_CLOSED_DEGREES);
  if (indicate) {
    setColorOfIndicator(COLOR_RED);
  }
  isLocked = true;
  saveCurrentState();
  sendLockStatus();
}

void saveCurrentState() {
  EEPROM.write(eeAddress, isLocked);
  boolean isLockedInMemory;
  EEPROM.get(eeAddress, isLockedInMemory);
  Serial.print(isLockedInMemory);
  EEPROM.commit();
}

void setColorOfIndicator(int color) {
  switch (color) {
    case COLOR_NONE:
      strip.setPixelColor(0, 0, 0, 0);
      strip.show();
      break;
    case COLOR_ORANGE:
      strip.setPixelColor(0, 255, 165, 0);
      strip.show();
      break;
    case COLOR_GREEN:
      Serial.println("Setting color to green");
      strip.setPixelColor(0, 0, 255, 0);
      strip.show();
      break;
    case COLOR_RED:
      Serial.println("Setting color to red");
      strip.setPixelColor(0, 255, 0, 0);
      strip.show();
      break;
    case COLOR_BLUE:
      Serial.println("Setting color to blue");
      strip.setPixelColor(0, 0, 255, 255);
      strip.show();
      break;
  }
}

void blink(int color) {
  setColorOfIndicator(color);
  delay(300);
  setColorOfIndicator(0);
}
